<div class="form-group">
    {!! Form::label('name','Name',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="iconic-input right">
            <i class="fa fa-star red"></i>
            {!! Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Enter name here']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('address','Address',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Enter address here']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('phone','Phone',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::number('phone',null,['class'=>'form-control','min'=>'0','placeholder'=>'Enter phone number here']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('email','Email',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="iconic-input right">
            <i class="fa fa-star red"></i>
            {!! Form::email('email',null,['class'=>'form-control','required','placeholder'=>'Enter email here']) !!}
        </div>
    </div>
</div>
@if(!isset($admin))
    <div class="form-group">
        {!! Form::label('password','Password',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
        <div class="col-lg-10 passwordDiv">
            <div class="iconic-input right">
                <i class="fa fa-star red"></i>
                {!! Form::password('password',['class'=>'form-control password','required','placeholder'=>'Enter password here']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('password_confirmation','Confirm Password',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
        <div class="col-lg-10 ">
            <div class="iconic-input right">
                <i class="fa fa-star red"></i>
                {!! Form::password('password_confirmation',['class'=>'form-control cPassword','required','placeholder'=>'Re-enter password here']) !!}
            </div>

        </div>
    </div>
@endif
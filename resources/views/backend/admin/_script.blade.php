<script>
    $(function () {
        $('#adminTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.list') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'phone', name: 'phone' },
                { data: 'email', name: 'email' },
                {data:'status',name:'status'},
                { data: 'actions', name: 'actions' }
            ]
        });

        $('.btnCreate').click(function () {
            var password= $('.password').val();
            var cPassword= $('.cPassword').val();
            $('.passwordMessageDiv').remove();
            if(password != cPassword)
            {
                $('.passwordDiv').append('<i class="passwordMessageDiv red">Password mismatch</i>')
                event.preventDefault();
            }
        });
    });


    // Status Active and Inactive
    $(document).on('click', '.status-modal', function() {
        $('#footer_action_button').text(" Change");
        $('#footer_action_button').addClass('fa-check');
        $('#footer_action_button').removeClass('fa-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-warning');
        $('.actionBtn').addClass('status');
        $('.modal-title').text('Status');
        $('.did').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.dname').html($(this).data('status'));
        $('#myModal').modal('show');
    });
    $('.modal-footer').on('click', '.status', function() {

        $.ajax({
            type: 'post',
            url: "{{route('admin.status')}}",
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $('.did').text(),
                'status':$('.dname').text()
            },
            success: function(data) {

                $('.itemcat' + data.status.id).replaceWith("<tr class='itemcat" + data.status.id + "'><td>" + data.status.id + "</td>" +
                        "<td>" + data.status.name + "</td>" +"<td>" + data.status.phone + "</td>"+"<td>" + data.status.email + "</td>"+
                        "<td>" +"<button data-id='" + data.status.id + "' data-status='" + data.status.status + "'   class='status-modal btn btn-" + data.message+ "'> " + data.status.status + "  </button></td>" +
                        "<td><a href='{{url('/')}}/admin/" + data.status.id + "/edit' class='edit-modal btn btn-primary'   data-id='" + data.status.id + "' data-name='" + data.status.name + "'><i class='fa fa-edit'></i></a> " +
                        " </td></tr>");
            }
        });
    });
</script>
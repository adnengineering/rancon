@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-offset-2 col-lg-7">
        <section class="panel">
            <header class="panel-heading">
                Create admin
            </header>
            <div class="panel-body">
                @include('layouts.backend._validationErrorMessages')
                {!! Form::model($admin,['route'=>['admin.update',$admin->id],'method'=>'put','class'=>'form-horizontal']) !!}
                    @include('backend.admin._form')
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            {!! Form::submit('Update',['class'=>'btn btn-success btnCreate']) !!}
                            <a href="{{ route('admin.index') }}" class="btn btn-warning pull-right"><< Back</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>
@endsection
@push('js')

@endpush
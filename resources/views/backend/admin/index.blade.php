@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Admin List
                </header>
                <div class="panel-body">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="adminTable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th class="hidden-phone">Email</th>
                                <th>Status</th>
                                <th class="hidden-phone">Actions</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </section>

            {{ csrf_field() }}
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                             <div class="deleteContent">
                                Are you Sure you want to Change Status <span class="dname"></span> ? <span
                                        class="hidden did"></span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn actionBtn" data-dismiss="modal">
                                    <span id="footer_action_button" class='fa'> </span>
                                </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">
                                    <span class='fa fa-times'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ asset('assets/data-table/dataTables.min.css') }}">
@endpush
@push('js')
    <script src="{{ asset('assets/data-table/dataTables.min.js') }}"></script>
    @include('backend.admin._script')
@endpush
<script>
    function countCompany(count)
    {
        var div_by = 100,
                speed = Math.round(count / div_by),
                $display = $('.countCompany'),
                run_count = 1,
                int_speed = 24;

        var int = setInterval(function() {
            if(run_count < div_by){
                $display.text(speed * run_count);
                run_count++;
            } else if(parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }
    var tcompany='{{$totalCompany}}';
    countCompany(parseInt(tcompany));


    function countProducts(count)
    {
        var div_by = 100,
                speed = Math.round(count / div_by),
                $display = $('.countProducts'),
                run_count = 1,
                int_speed = 24;

        var int = setInterval(function() {
            if(run_count < div_by){
                $display.text(speed * run_count);
                run_count++;
            } else if(parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }
    var tproducts='{{$totalProducts}}';
    countProducts(parseInt(tproducts));




    function countSales(count)
    {
        var div_by = 1000,
                speed = Math.round(count / div_by),
                $display = $('.countSales'),
                run_count = 1,
                int_speed = 24;

        var int = setInterval(function() {
            if(run_count < div_by){
                $display.text(speed * run_count);
                run_count++;
            } else if(parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }
    var tsales='{{$totalSales}}';
    countSales(tsales);



    function countProfit(count)
    {
        var div_by = 1000,
                speed = Math.round(count / div_by),
                $display = $('.countProfit'),
                run_count = 1,
                int_speed = 24;

        var int = setInterval(function() {
            if(run_count < div_by){
                $display.text(speed * run_count);
                run_count++;
            } else if(parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }

    var tprofit='{{$totalprofit}}'
    countProfit(tprofit);

</script>
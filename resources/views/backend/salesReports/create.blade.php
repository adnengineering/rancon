@extends('layouts.backend.master')
@section('content')
    <div class="row">
        <div class="col-md-offset-2 col-lg-7">
            <section class="panel">
                <header class="panel-heading">
                    Create  Sales Report
                </header>
                <div class="panel-body">
                    @include('layouts.backend._validationErrorMessages')
                    {!! Form::open(['route'=>'sales_report.store','class'=>'form-horizontal']) !!}
                    @include('backend.salesReports._form')
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            {!! Form::submit('Create',['class'=>'btn btn-success btnCreate']) !!}
                            <a href="{{ route('sales_report.index') }}" class="btn btn-warning pull-right"><< Back</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection
@push('css')

<!--select 2-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/select2/css/select2.min.css') }}"/>
@endpush
@push('js')


<!--select2-->
<script type="text/javascript" src="{{ asset('assets/select2/js/select2.min.js') }}"></script>
@include('backend.salesReports._script')
@endpush
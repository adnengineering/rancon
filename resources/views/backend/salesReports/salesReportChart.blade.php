@extends('layouts.backend.master')
@section('content')
        <!-- page start-->
<div id="morris">
    <div class="row">

        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sales Report By Brand
                </header>
                <div class="panel-body">

                    <div id="hero-bar" class="graph"></div>
                </div>
            </section>
        </div>
    </div>
    <input type="hidden" id="url" value="{{ url('brand') }}">
</div>

<!-- page end-->
@endsection
@push('css')
<link href="{{ asset('assets/morris.js-0.4.3/morris.css') }}" rel="stylesheet" />
<style>

text{
        font: 20px Verdana, Helvetica, Arial, sans-serif;
    }
.morris-default-style{
    cursor: pointer !important;
}
    tspan{
        fill: rgba(20, 17, 29, 0.49);
        font-weight: bold;

    }

</style>
@endpush
@push('js')
<script src="{{ asset('assets/morris.js-0.4.3/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/morris.js-0.4.3/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/morris-script.js') }}"></script>
<script>
    var Script = function () {

        //morris chart

        $(function () {
            Morris.Bar({
                element: 'hero-bar',
                data:  [  @foreach($record[0] as $v){brand: '{{$v->brand}}',id : '{{$v->brand_id}}', grossProfitAmount : '{{$v->grossProfitAmount}}' ,unit:'{{$v->unit}}'   },@endforeach ],
                xkey: 'brand',
                ykeys: ['grossProfitAmount','unit'],
                labels: ['Gross Profit','Unit'],
                barRatio: 0.4,
                hideHover: 'auto',
                barColors: ['#6883a3'],
                stacked: true
            }).on('click', function(i, row){
                var url = $('#url').val();
                window.location.href= url+'/'+row.id+'/products';

            });


            $('.code-example').each(function (index, el) {
                eval($(el).text());

            });
        });

    }();


</script>
@endpush

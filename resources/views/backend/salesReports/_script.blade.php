<script>
    $(function(){
        $(".cal_val").on("change paste keyup", function() {
            $(".ap_total").val("");
            var netsalesa = $("input[name='netSalesAmount']").val();
            var costofsales = $("input[name='costOfSalesAmount']").val();
            var t = netsalesa - costofsales;
            $(".ap_total").val(function() {
                return this.value + t;
            })
        })
    })


    $(document).ready(function () {
        $(".js-example-basic-single").select2();

    });
</script>
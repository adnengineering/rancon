@extends('layouts.backend.master')
@section('content')
        <!-- page start-->
<div id="morris">
    <div class="row">

        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sales Report By Products <a class="btn btn-success pull-right" href="{{url()->previous()}}"><  Back</a>
                </header>
                <div class="panel-body">

                    <div id="hero-bar" class="graph"></div>
                </div>
            </section>
        </div>
    </div>

</div>

<!-- page end-->
@endsection
@push('css')
<link href="{{ asset('assets/morris.js-0.4.3/morris.css') }}" rel="stylesheet" />
<style>

    text{
        font: 20px Verdana, Helvetica, Arial, sans-serif;
    }

    tspan{
        fill: rgba(20, 17, 29, 0.49);
        font-weight: bold;
        height: auto;
    }

</style>
@endpush
@push('js')
<script src="{{ asset('assets/morris.js-0.4.3/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/morris.js-0.4.3/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/morris-script.js') }}"></script>
<script>
    var Script = function () {

        //morris chart

        $(function () {
            Morris.Bar({
                element: 'hero-bar',
                data:  [  @foreach($record[0] as $v){pname: '{{$v->pname}}', unit : '{{$v->unit}}',grossProfitAmount:'{{strval($v->grossProfitAmount)}}'   },@endforeach ],
                xkey: 'pname',
                ykeys: ['unit','grossProfitAmount'],
                labels: ['Unit','Profit Amount'],
                barRatio: 0.4,
                hideHover: 'auto',
                barColors: ['#6883a3'],
                stacked: true
            }).on('load', function(i, row){
                console.log(i, row.brand);
                alert(row.brand);
                $('tspan').html(row.brand);

            });


            $('.code-example').each(function (index, el) {
                eval($(el).text());

            });
        });

    }();

</script>
@endpush

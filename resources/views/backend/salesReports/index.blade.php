@extends('layouts.backend.master')
@section('content')
    <!-- page start-->
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Dynamic Table
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="row">
                        {{ Form::open(['method'=>'get']) }}
                        <div class="col-md-3 col-md-offset-1">
                            {{ Form::Select('company_id',$companies,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Select Company']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::Select('brand_id',$brands,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Select Brand']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::Select('product_id',$products,null,['class'=>'form-control js-example-basic-single','placeholder'=>'Select Product']) }}
                        </div>
                        <div class="col-md-2">

                            {{ Form::button('<i class="fa fa-search"></i> Search',['type'=>'submit','class'=>'btn btn-primary']) }}

                        </div>
                        {{ Form::close() }}
                    </div>
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped table-responsive data-table" id="dynamic-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company</th>
                                <th>Brand</th>
                                <th class="hidden-phone">Product</th>
                                <th class="hidden-phone">Unit</th>
                                <th>Net Sales Amount</th>
                                <th>Cost Of Sales Amount</th>
                                <th>Gross Profit</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all as  $k=>$v)
                                <tr class="gradeX">
                                    <td>{{$loop->iteration}}</td>
                                    <td> {{\Illuminate\Support\Facades\DB::table('company')->where('id',$v->company_id)->first()->name}}</td>
                                    <td>{{\Illuminate\Support\Facades\DB::table('brand')->where('id',$v->brand_id)->first()->name}}</td>
                                    <td>{{\Illuminate\Support\Facades\DB::table('products')->where('id',$v->product_id)->first()->name}}</td>
                                    <td>{{$v->unit}}</td>
                                    <td>{{number_format($v->netSalesAmount)}} &#x9f3;</td>
                                    <td>{{number_format($v->costOfSalesAmount)}} &#x9f3;</td>
                                    <td>{{number_format($v->grossProfitAmount)}} &#x9f3;</td>
                                    <td>
                                        <a href="#"><i class="fa fa-pencil-square"></i> Edit</a>
                                        ||
                                        <a href="#"><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Company</th>
                                <th>Brand</th>
                                <th class="hidden-phone">Product</th>
                                <th class="hidden-phone">Total:{{$all->sum('unit')}}</th>
                                <th>Total : {{number_format($all->sum('netSalesAmount'))}} &#x9f3;</th>
                                <th>Total : {{number_format($all->sum('costOfSalesAmount'))}} &#x9f3;</th>
                                <th>Total : {{number_format($all->sum('grossProfitAmount'))}} &#x9f3;</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@push('css')
        <!--select 2-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/data-tables/DT_bootstrap.css') }}" />

    <link href="{{ asset('assets/advanced-datatable/media/css/demo_table.css') }}" rel="stylesheet" />
@endpush
@push('js')
    <script type="text/javascript" language="javascript" src="{{ asset('assets/advanced-datatable/media/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/data-tables/DT_bootstrap.js') }}"></script>

    <!--dynamic table initialization -->
    <script src="{{ asset('js/dynamic_table_init.js') }}"></script>
<!--select2-->
<script type="text/javascript" src="{{ asset('assets/select2/js/select2.min.js') }}"></script>
@include('backend.salesReports._script')
@endpush
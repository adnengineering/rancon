<div class="form-group">
    {!! Form::label('name','Comapny',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="iconic-input right">

            {{ Form::select('company_id',$companylist,null,['class' => 'form-control js-example-basic-single','required'=>'1','placeholder'=>"Select Company"]) }}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('name','Brand',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="iconic-input right">

            {{ Form::select('brand_id',$brandlist,null,['class' => 'form-control js-example-basic-single','required'=>'1','placeholder'=>"Select Brand"]) }}

        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('name','Product',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        <div class="iconic-input right">

            {{ Form::select('product_id',$productlist,null,['class' => 'form-control js-example-basic-single','required'=>'1','placeholder'=>"Select Product"]) }}

        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('unit','Unit',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::number('unit',null,['class'=>'form-control','min'=>'0','placeholder'=>'Enter Number of Unit']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('netSalesAmount','Net Sales Amount',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::number('netSalesAmount',null,['class'=>'form-control cal_val','min'=>'0','placeholder'=>'Enter Net Sales Amount']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('costOfSalesAmount','Cost Of Sales Amount',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::number('costOfSalesAmount',null,['class'=>'form-control cal_val','min'=>'0','placeholder'=>'Enter Cost Of Amount']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('grossProfitAmount','Gross Profit',['class'=>'col-lg-2 col-sm-2 control-label']) !!}
    <div class="col-lg-10">
        {!! Form::number('grossProfitAmount',null,['class'=>'form-control ap_total','min'=>'0','placeholder'=>'Total Gross Profit','readonly']) !!}
    </div>
</div>

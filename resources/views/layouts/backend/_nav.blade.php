<div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        <li>
            <a class="{{ request()->segment(1) == 'dashboard' ? 'active' : null }}" href="{!! route('dashboard') !!}">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'admin' ? 'active' : null }}" >
                <i class="fa fa-user"></i>
                <span>Admins</span>
            </a>
            <ul class="sub">
                <li class="{{ request()->segment(2) == '' ? 'active' : null }}"><a href="{{ route('admin.index') }}">Lists</a></li>
                <li class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a href="{{ route('admin.create') }}">Create</a></li>
            </ul>
        </li>
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'reportOne' || request()->segment(1) == 'reportTwo' || request()->segment(1) == 'reportThree' || request()->segment(1) == 'reportFour' ? 'active' : null }}">
                <i class=" fa fa-bar-chart-o"></i>
                <span>Report</span>
            </a>
            <ul class="sub">
                <li class="{{ request()->segment(1) == 'reportOne' ? 'active' : null }}" ><a href="{!! route('reportOne') !!}">Report Demo 1</a></li>
                <li class="{{ request()->segment(1) == 'reportTwo' ? 'active' : null }}" ><a href="{!! route('reportTwo') !!}">Report Demo 2</a></li>
                <li class="{{ request()->segment(1) == 'reportThree' ? 'active' : null }}" ><a href="{!! route('reportThree') !!}">Report Demo 3</a></li>
                <li class="{{ request()->segment(1) == 'reportFour' ? 'active' : null }}" ><a href="{!! route('reportFour') !!}">Report Demo 4</a></li>
            </ul>
        </li>
        <li class="sub-menu">
            <a href="javascript:;" class="{{ request()->segment(1) == 'sales_report' ? 'active' : null }}">
                <i class=" fa fa-bar-chart-o"></i>
                <span>Sales Report</span>
            </a>
            <ul class="sub">
                <li  class="{{ request()->segment(2) == 'create' ? 'active' : null }}"><a  href="{{ route('sales_report.create') }}">Create</a></li>
                <li class="{{ request()->segment(2) == '' ? 'active' : null }}"><a  href="{!! route('sales_report.index') !!}">Lists</a></li>
                <li class="{{request()->segment(1) == '' ? 'active' : null }}"><a  href="{!! route('sales.report') !!}">Sales Chart</a></li>

            </ul>
        </li>
    </ul>
    <!-- sidebar menu end-->
</div>
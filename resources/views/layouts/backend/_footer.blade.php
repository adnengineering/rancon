<div class="text-center">
   <?php echo date('Y'); ?> &copy;  {{ config('app.name') }}.
    <a href="#" class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>
</div>
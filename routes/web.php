<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/reportOne', function () {
                $t=\Illuminate\Support\Facades\DB::table('morris_chart')->select('device','geekbench')->get();
    $record= array($t);
  //  dd($record);
   // $record = $this->convertToString($r);
    return view('report.demo1',['record'=>$record]);
})->name('reportOne');

Route::get('/reportTwo', function () {
    return view('report.demo2');
})->name('reportTwo');

Route::get('/reportThree', function () {
    return view('report.demo3');
})->name('reportThree');

Route::get('/reportFour', function () {
    return view('report.demo4');
})->name('reportFour');


Route::namespace('Backend')->group(function (){
    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::resource('admin', 'AdminController', ['only' => [
        'index', 'create' , 'store', 'edit', 'update'
    ]]);
    Route::get('admin/list','AdminController@ajaxList')->name('admin.list');
    Route::post('admin/status','AdminController@changeStatus')->name('admin.status');

    Route::resource('sales_report','SalesReportController', ['only' => [
        'index', 'create' , 'store', 'edit', 'update'
    ]]);

    Route::get('sales/report','SalesReportController@getChartReport')->name('sales.report');
    Route::get('brand/{bid}/products','SalesReportController@bandByChartReport')->name('brand.products');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

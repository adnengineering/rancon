<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReport extends Model
{
    //
    protected  $table="sales_reports";
    protected $fillable=['company_id','brand_id','product_id','unit','netSalesAmount','costOfSalesAmount','grossProfitAmount'];
}

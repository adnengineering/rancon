<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $data['totalCompany']=DB::table('company')->count();
        $data['totalProducts']=DB::table('products')->count();
        $data['totalSales']=DB::table('sales_reports')->sum('netSalesAmount');
        $data['totalprofit']=DB::table('sales_reports')->sum('grossProfitAmount');
        return view('backend/dashboard',$data);
    }
}

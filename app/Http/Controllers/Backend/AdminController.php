<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function index()
    {
        $data['title']='List of admin';
        return view('backend.admin.index',$data);
    }
    public function ajaxList()
    {
        $admins=User::all();

        return Datatables::of($admins)->addColumn('status', function($admins) {
            if($admins->status=='Active'){
                return '<button data-id="'.$admins->id.'" data-status="'.$admins->status.'"   class="status-modal btn btn-success"> <i class="fa fa-check-square-o"></i>Active  </button>';
               }
            else{
                return '<button data-id="'.$admins->id.'" data-status="'.$admins->status.'"    class="status-modal btn btn-danger"><i class="fa fa-times"></i> Inactive  </button>';
            }

        })->addColumn('actions',function ($admins){
            return '<a href="'.route("admin.edit",$admins->id).'" class="btn btn-primary"><i class="fa fa-edit"></i></a>';
        })->rawColumns(['Status'=>'status','actions'=>'actions'])->setRowClass(function ($admins) {
            return 'itemcat'.$admins->id;
        })->make(true);
    }
    public function create()
    {
        $data['title']='Create admin';
        return view('backend.admin.create',$data);
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|confirmed',
        ]);
        $user=new User();
        $user->name=$request->name;
        $user->address=$request->address;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->save();
        Session::flash('message', 'Admin successfully created.');
        return redirect()->back();
    }
    public function edit($id)
    {
        $data['title']='Edit admin';
        $data['admin']=User::findOrFail($id);
        return view('backend.admin.edit',$data);
    }
    public function update(Request $request,$id)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
        ]);
        $user=User::findOrFail($id);
        $user->name=$request->name;
        $user->address=$request->address;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->save();
        Session::flash('message', 'Admin successfully updated.');
        return redirect()->back();
    }

    public function changeStatus(Request $req)
    {
        $status= User::find($req->id);
        if($req->status=='Active')
        {
            $status->status='Inactive';
            $message="danger";
        }
        else{
            $status->status='Active';
            $message="success";
        }
        $status->save();
        return response ()->json ( ['status'=>$status,'message'=>$message] );
    }
}

<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\SalesReport;
use Session;
class SalesReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $all=new SalesReport();
        if($request->has('company_id') && $request->company_id != null)
        {
            $all=$all->where('company_id',$request->company_id);
        }
        if($request->has('brand_id') && $request->brand_id != null)
        {
            $all=$all->where('brand_id',$request->brand_id);
        }
        if($request->has('product_id') && $request->product_id != null)
        {
            $all=$all->where('product_id',$request->product_id);
        }
        $data['all']=$all->get();
        $data['companies']=DB::table('company')->pluck('name','id');
        $data['brands']=DB::table('brand')->pluck('name','id');
        $data['products']=DB::table('products')->pluck('name','id');
        return view('backend.salesReports.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companylist=DB::table('company')->pluck('name','id');
        $brandlist=DB::table('brand')->pluck('name','id');
        $productlist=DB::table('products')->pluck('name','id');
       return view('backend.salesReports.create',['companylist'=>$companylist,
           'brandlist'=>$brandlist,
           'productlist'=>$productlist]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       //dd($request->all());
        $salesReportsave=SalesReport::insert(['company_id'=>$request->company_id,
            'brand_id'=>$request->brand_id,
            'product_id'=>$request->product_id,
            'unit'=>$request->unit,
            'netSalesAmount'=>$request->netSalesAmount,
            'costOfSalesAmount'=>$request->costOfSalesAmount,
            'grossProfitAmount'=>$request->grossProfitAmount]);
        Session::flash('message', 'Sales Report successfully created.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getChartReport()
    {
        /*
         * SELECT sum(unit),sum(grossProfitAmount), brand_id
            FROM sales_reports
            GROUP BY brand_id;
         * */
//         $t=SalesReport::select('brand_id','unit','grossProfitAmount','brand.name as brand')
         /*$t=DB::table('sales_reports')->select('sales_reports.brand_id','sales_reports.unit','sales_reports.grossProfitAmount')
//             ->join('brand', 'brand.id', '=', 'sales_reports.brand_id')
             ->where('company_id',2)
             ->groupBy('brand_id')
             ->sum('sales_reports.grossProfitAmount');
//             ->get();
//             ->count();*/
        $t=DB::select("SELECT sum(unit) as unit,sum(grossProfitAmount) as grossProfitAmount, brand_id, brand.name as brand FROM sales_reports JOIN brand ON sales_reports.brand_id=brand.id GROUP BY brand_id,brand");
//         dd($t);
        $record= array($t);
       return view('backend.salesReports.salesReportChart',['record'=>$record]);
    }

    public function bandByChartReport($id)
    {

          $t=SalesReport::select('unit','grossProfitAmount','products.name as pname')
            ->join('products', 'products.id', '=', 'sales_reports.product_id')->where('sales_reports.brand_id',$id)->get();
       // $t=DB::select("SELECT sum(unit) as unit,sum(grossProfitAmount) as grossProfitAmount, brand_id, brand.name as brand FROM sales_reports JOIN brand ON sales_reports.brand_id=brand.id GROUP BY brand_id,brand");
//         dd($t);
        $record= array($t);
        return view('backend.salesReports.chartBrandByProduct',['record'=>$record]);
    }
}

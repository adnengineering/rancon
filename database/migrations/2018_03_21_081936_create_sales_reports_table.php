<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('brand_id');
            $table->integer('product_id');
            $table->integer('unit');
            $table->float('netSalesAmount', 8, 2);
            $table->float('costOfSalesAmount', 8, 2);
            $table->float('grossProfitAmount', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_reports');
    }
}
